import {
    defaultFont,
    roseColor
} from "../_should-be-moved-to-baseTheme_material-dashboard-react";

const signInStyle = theme => ({
    root: {
        width: '100vw',
        height: '100vh',
        "& *": {
            ...defaultFont
        }
    },
    gridContainer: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: 'center',
        height: '100%',
        width: '100%',
    },
    gridItem: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: 'center',
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: 'auto',
        },
    },
    paper: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
            .spacing.unit * 3}px`,
        width: '100%',
        height: '100%',
        minWidth: '500px',
        minHeight: '300px',
        position: "relative",
        [theme.breakpoints.up('sm')]: {
            height: 'auto',
            marginTop: theme.spacing.unit * 2,
            marginBottom: theme.spacing.unit * 2
        },
    },
    stepsHeader: {
        width: '100%',
        minHeight: '80px',
        maxHeight: '80px',
    },
    steps: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        width: '100%',
        height: '100%',
        position: 'relative',
    },
    avatar: {
        backgroundColor: roseColor,
        color: "#fff",
        width: 60,
        height: 60,
        position: "absolute",
        top: "-30px",
        left: "30px",
        borderRadius: "3px"
    },
    form: {
        marginTop: theme.spacing.unit,
        width: '100%',
        height: '100%',
        position: 'relative',
        [theme.breakpoints.up('sm')]: {
            height: 'auto',
        },
    },
    formErrorSucc: {
        color: 'green',
    },
    formErrorFail: {
        color: 'red',
    },
    divider: {
        display: "flex",
        justifyContent: "center",
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        [theme.breakpoints.up('sm')]: {
            position: 'relative',
        },
    },
    link: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        width: '100%',
    },
    submit: {
        paddingLeft: "30px",
        paddingRight: "30px",
        color: "#fff",
        backgroundColor: roseColor
    },
    iconNext: {
        // display: 'none',
        display: 'inline-block',
        // '&$submit:hover': {
        //     display: 'inline-block'
        // }
    }
});

export default signInStyle;
