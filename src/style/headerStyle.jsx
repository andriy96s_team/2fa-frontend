import {roseColor} from "../_should-be-moved-to-baseTheme_material-dashboard-react";

const headerStyle = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: roseColor,
        colorPrimary: roseColor,
        colorDefault: roseColor
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },


});

export default headerStyle;