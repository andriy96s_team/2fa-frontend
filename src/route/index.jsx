import Hello from "../component/Hello";
import SignUp from "../component/SignUp";
import Settings from "../component/Settings";
import SignIn from "../component/SignIn";


const indexRoutes = [
    {path: "/login", component: SignIn},
    {path: "/signup", component: SignUp},
    {path: "/settings", component: Settings},
    {path: "/", component: Hello}
];

export default indexRoutes;