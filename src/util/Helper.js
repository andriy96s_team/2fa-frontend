export default function getLoggedInUser() {
    const auth = localStorage.getItem("auth");
    if (auth) {
        return JSON.parse(auth)
    }
    return null
}

export function getLoggedInUserLogin() {
    const auth = localStorage.getItem("auth");
    console.log("1 getLoggedInUserLogin", auth)
    let userLogin = null;
    if (auth) {
        const authParsed = JSON.parse(auth);
        console.log("2 getLoggedInUserLogin", authParsed)
        if (authParsed) {
            userLogin = authParsed.username;
        }
    }
    console.log("4 getLoggedInUserLogin ret", userLogin)
    return userLogin;
}

export function LogoutCurrentUserIfExists(redirectTo, router) {
    const auth = localStorage.getItem("auth");
    if (auth) {
        const authParsed = JSON.parse(auth);
        if (authParsed) {
                localStorage.removeItem("auth");
        }
    }
}

export function getBackendUrl() {
  return "https://backend-2fa.herokuapp.com"
  // return "http://localhost:8080"
}
