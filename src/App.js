import React from 'react';
import './App.css';
import Application from "./component/Application";
import {createBrowserHistory} from "history";
import {SnackbarProvider, withSnackbar} from 'notistack';
import CssBaseline from "@material-ui/core/CssBaseline";
import indexRoutes from "./route";
import {Route, Router, Switch} from "react-router-dom";
import getLoggedInUser from "./util/Helper";


class App extends React.Component {

    constructor(props) {
        super(props);
        this.setLoggedInUser = this.setLoggedInUser.bind(this);
    }

    hist = createBrowserHistory();
    state = {loggedInUser: null};

    setLoggedInUser(value) {
        this.setState({loggedInUser: value});
    }

    switchRoutes = (
        <Switch {...this.state}>
            {indexRoutes.map((prop, key) => {
                // return <Route path={prop.path} component={prop.component} key={key} {...prop}></Route>;
                return <Route path={prop.path}
                              render={(routeProps) => {
                                  const TagName = prop.component;

                                  return <TagName component={withSnackbar(prop.component)}
                                                  setLoggedInUser={this.setLoggedInUser}
                                                  history={this.hist} {...this.state}/>;
                              }}
                              key={key}/>;
            })}
            {/*<Route component={index404Page} />*/}
        </Switch>
    );

    componentDidMount() {
        this.setState({loggedInUser: getLoggedInUser()});
    }

    render(): React.ReactNode {
        return (
            <Router history={this.hist}>
                    <React.Fragment>
                        <CssBaseline/>
                        <SnackbarProvider maxSnack={7}>
                        <Application history={this.hist} setLoggedInUser={this.setLoggedInUser} {...this.state}>
                            {this.switchRoutes}
                        </Application>
                        </SnackbarProvider>
                    </React.Fragment>
            </Router>);
    }
}

export default App;
