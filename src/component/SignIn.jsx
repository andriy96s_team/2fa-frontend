import React from "react";
import Login from "./Login";
import TgCode from "./TgCode";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import signInStyle from "../style/signInStyle";
import {withSnackbar} from "notistack";

class SignIn extends React.Component {

    state = {
        username: null,
        is2FA: false
    };

    constructor(props) {
        super(props);
        this.setValue = this.setValue.bind(this);
    }

    setValue(name, value) {
        this.setState({
            [name]: value,
        });
    }


    render(): React.ReactNode {
        return (
            this.state.is2FA ? (
                    <TgCode
                        username={this.state.username}
                        setValue={this.setValue}
                        {...this.props}/>
                )
                : (
                    <Login
                        setValue={this.setValue}
                        {...this.props}/>
                )
        )
    }
}

SignIn.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withSnackbar(withStyles(signInStyle)(SignIn));