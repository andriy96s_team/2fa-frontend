import React from "react";
import PropTypes from "prop-types";
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
import {Button, FormControl, FormHelperText, Grid, Input, InputLabel, Paper} from "@material-ui/core";

import signUpStyle from "../style/signUpStyle";
import {getBackendUrl} from "../util/Helper";
import {withSnackbar} from "notistack";


const errorCodeToMessageMappings = code => {
    if (!code) {
        return null
    }

    switch (code) {
        case 400:
        case 401:
        case 402:
        case 403:
        case 404:
        case 501:
            return "Error " + code;

        default:
            return "Undefined error";
    }
};

const emptyState = {
    username: null,
    firstName: null,
    lastName: null,
    password: null,
    confirmPassword: null,
    errors: {},
    hideIconPassword: true
};

class SignUp extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            ...emptyState
        };

    }

    onChange = (id, value) => {
        this.setState({[id]: value})
    };

    // checkFiledValidations(sponsor, userName, firstName, lastName, email, country, password, confirmPassword) {
    //     let errors = this.state.errors;
    //     let isValidationFailed = false;
    //
    //     if (!sponsor) {
    //         isValidationFailed = true;
    //         errors.sponsor = 400;
    //     } else {
    //         errors.sponsor = null;
    //     }
    //
    //     if (!userName) {
    //         isValidationFailed = true;
    //         errors.userName = 400;
    //     } else {
    //         errors.userName = null;
    //     }
    //
    //     if (!email /*|| !"...+\@...+\....+".match(email)*/) {
    //         isValidationFailed = true;
    //         errors.email = 401;
    //     } else {
    //         errors.email = null;
    //     }
    //
    //     if (!country) {
    //         isValidationFailed = true;
    //         errors.country = 400;
    //     } else {
    //         errors.country = null;
    //     }
    //
    //     if (!password) {
    //         isValidationFailed = true;
    //         errors.password = 400;
    //     } else {
    //         errors.password = null;
    //     }
    //
    //     if (password && password.localeCompare(confirmPassword) !== 0) {
    //         isValidationFailed = true;
    //         errors.confirmPassword = 402;
    //     } else {
    //         errors.confirmPassword = null;
    //     }
    //     this.setState({errors});
    //     return isValidationFailed;
    // }

    handleSubmitAction = event => {
        event.preventDefault();

        const {username, firstName, lastName, password} = this.state;

        // if (this.checkFiledValidations( username, firstName, lastName, password, confirmPassword)) {
        //     return
        // }

        const body = {
            username,
            firstName,
            lastName,
            password
        };

        console.log(body);

        // const currLocation = window.location;
        fetch(`${getBackendUrl()}/api/user/signup`, {
            headers: {
                "Content-Type": "application/json;charset=UTF-8"
            },
            method: "POST",
            // credentials: "omit",
            body: JSON.stringify(body)
        }).then(response => {

            if (response.status === 200) {
                response.json().then(data => {
                    this.props.enqueueSnackbar("You successfully finished registration", {variant: 'success'});
                    this.props.history.push('/')
                })
            } else if (response.status === 400) {
                response.json().then(data => {
                    this.setState({
                        errors: data,
                    })
                })
            } else {
                response.json().then(data => {
                    console.error(response.code, data)
                })
            }
        });
    };

    render() {
        const {classes} = this.props;
        const {
            username,
            firstName,
            lastName,
            password,
            confirmPassword,
            errors,
        } = this.state;
        return (
            <div className={classes.root}>
                <Grid container className={classes.gridContainer}>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <div className={classes.card}>
                                <div color="info" className={classes.cardHeader}>
                                    <div>SignUp</div>
                                </div>
                                <div>
                                    <form onSubmit={this.handleSubmitAction}>
                                        <Grid container
                                            // alignItems={'stretch'}
                                              direction={'column'}
                                            // justify={'center'}
                                              className={classes.gridContainer}
                                        >
                                            <Grid item xs={12} className={classes.gridItem}>
                                                <FormControl margin="normal" fullWidth required>
                                                    <InputLabel htmlFor="username">
                                                        userName
                                                    </InputLabel>
                                                    <Input
                                                        id="username"
                                                        name="username"
                                                        autoComplete="username"
                                                        value={username}
                                                        onChange={(event => this.onChange('username', event.target.value))}
                                                    />
                                                    {errors.username ? (
                                                        <FormHelperText className={classes.formErrorFail}>
                                                            {errorCodeToMessageMappings(errors.username)}
                                                        </FormHelperText>
                                                    ) : null}
                                                </FormControl>
                                            </Grid>
                                            <Grid item xs={12} className={classes.gridItem}>
                                                <FormControl margin="normal" fullWidth required>
                                                    <InputLabel htmlFor="firstName">
                                                        First Name
                                                    </InputLabel>
                                                    <Input
                                                        id="firstName"
                                                        name="firstName"
                                                        autoComplete="firstName"
                                                        value={firstName}
                                                        onChange={(event => this.onChange('firstName', event.target.value))}
                                                    />
                                                    {errors.firstName ? (
                                                        <FormHelperText className={classes.formErrorFail}>
                                                            {errorCodeToMessageMappings(errors.firstName)}
                                                        </FormHelperText>
                                                    ) : null}
                                                </FormControl>
                                            </Grid>
                                            <Grid item xs={12} className={classes.gridItem}>
                                                <FormControl margin="normal" fullWidth required>
                                                    <InputLabel htmlFor="lastName">
                                                        Last Name
                                                    </InputLabel>
                                                    <Input
                                                        id="lastName"
                                                        name="lastName"
                                                        autoComplete="lastName"
                                                        value={lastName}
                                                        onChange={(event => this.onChange('lastName', event.target.value))}
                                                    />
                                                    {errors.lastName ? (
                                                        <FormHelperText className={classes.formErrorFail}>
                                                            {errorCodeToMessageMappings(errors.lastName)}
                                                        </FormHelperText>
                                                    ) : null}
                                                </FormControl>
                                            </Grid>
                                            <Grid item xs={12} className={classes.gridItem}>
                                                <FormControl margin="normal" fullWidth required>
                                                    <InputLabel htmlFor="password">
                                                        Password
                                                    </InputLabel>
                                                    <Input
                                                        id="password"
                                                        name="password"
                                                        autoComplete="password"
                                                        value={password}
                                                        type={'password'}
                                                        onChange={(event => this.onChange('password', event.target.value))}
                                                    />
                                                    {errors.password ? (
                                                        <FormHelperText className={classes.formErrorFail}>
                                                            {errorCodeToMessageMappings(errors.password)}
                                                        </FormHelperText>
                                                    ) : null}
                                                </FormControl>
                                            </Grid>
                                            <Grid item xs={12} className={classes.gridItem}>
                                                <FormControl margin="normal" fullWidth required>
                                                    <InputLabel htmlFor="confirmPassword">
                                                        Confirm Password
                                                    </InputLabel>
                                                    <Input
                                                        id="confirmPassword"
                                                        name="confirmPassword"
                                                        autoComplete="confirmPassword"
                                                        value={confirmPassword}
                                                        type={'password'}
                                                        onChange={(event => this.onChange('confirmPassword', event.target.value))}
                                                    />
                                                    {errors.confirmPassword ? (
                                                        <FormHelperText className={classes.formErrorFail}>
                                                            {errorCodeToMessageMappings(errors.confirmPassword)}
                                                        </FormHelperText>
                                                    ) : null}
                                                </FormControl>
                                            </Grid>
                                        </Grid>
                                        <Grid item
                                              container
                                              alignItems={'center'}
                                              justify={'center'}
                                              direction={'row'}
                                        >
                                            <Button
                                                type="submit"
                                                variant="contained"
                                                className={classes.submit}
                                            >
                                                Sign Up
                                            </Button>
                                        </Grid>
                                    </form>
                                </div>
                            </div>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

SignUp.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withSnackbar(withStyles(signUpStyle)(SignUp));