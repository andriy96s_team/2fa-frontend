import React from "react";
import {Button, FormControl, Grid, Input, InputLabel, Paper} from "@material-ui/core";
import Checkbox from "@material-ui/core/Checkbox";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import signInStyle from "../style/signInStyle";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Typography from "@material-ui/core/Typography";
import {getBackendUrl, getLoggedInUserLogin} from "../util/Helper";
import {withSnackbar} from "notistack";

class Settings extends React.Component {
    state = {
        twoFAEnabled: false,
        tgId: null
    };

    componentDidMount() {
        const username = getLoggedInUserLogin();
        fetch(`${getBackendUrl()}/api/user/getSettings/${username}`, {
            headers: {
                "Content-Type": "application/json;charset=UTF-8"
            },
            method: "GET",
        }).then(response => {
            if (response.status === 200) {
                response.json().then(data => {
                    console.log(data);
                    this.setState({twoFAEnabled: data.twoFAEnabled, tgId: data.tgId});
                });
            } else {
                this.setState({
                    error: response.status,
                    errorEmailObject: {
                        reason: "User not found"
                    }
                })
            }
        });
    }

    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <Grid container className={classes.gridContainer} minWidth="75%">
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <div className={classes.card}>
                                <div color="info" className={classes.cardHeader}>
                                    <div>Settings</div>
                                </div>
                                <div>
                                    <form className={classes.form} onSubmit={this.handleSubmitAction}>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    checked={this.state.twoFAEnabled}
                                                    onChange={this.handleCheckboxChange.bind(this)}
                                                    value="twoFAEnabled"
                                                    id="twoFAEnabled"
                                                />
                                            }
                                            label="Enable two factor authorization"
                                        />
                                        <div hidden={!this.state.twoFAEnabled} className={classes.container}>
                                            <Typography>To enable tow factor authorization you have to go </Typography>
                                            <Typography>to telegram bot <a href="https://t.me/TFA_andriy96s_Bot"
                                                                           target="_blank"
                                                                           rel="noopener noreferrer">2FA Bot</a> and
                                                type /register command to get your ID </Typography>

                                            <FormControl margin="normal" required fullWidth>
                                                <InputLabel htmlFor="password">Telegram ID</InputLabel>
                                                <Input
                                                    name="tgId"
                                                    type="text"
                                                    id="tgId"
                                                    value={this.state.tgId}
                                                    onChange={this.handleTgIdChange.bind(this)}
                                                />
                                            </FormControl>
                                        </div>
                                        <div className={classes.divider}>
                                            <Button
                                                type="submit"
                                                variant="contained"
                                                className={classes.submit}
                                                disabled={!this.state.tgId && this.state.twoFAEnabled}
                                            >
                                                Save changes
                                            </Button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        )
    }

    handleSubmitAction = event => {
        const {twoFAEnabled, tgId} = this.state;
        event.preventDefault();

        const body = {
            "username": getLoggedInUserLogin(),
            "twoFAEnabled": twoFAEnabled,
            "tgId": tgId
        };

        // const currLocation = window.location;
        fetch(`${getBackendUrl()}/api/user/change2FA`, {
            headers: {
                "Content-Type": "application/json;charset=UTF-8"
            },
            method: "PUT",
            body: JSON.stringify(body)
        }).then(response => {

            if (response.status === 200) {
                this.props.enqueueSnackbar("Settings was saved", {variant: 'success'});
                this.props.history.push('/');
            } else if (response.status === 404) {
                this.setState({
                    error: response.status,
                    errorEmailObject: {
                        reason: "User not found"
                    }
                })
            } else {
                response.json().then(data => {
                    console.log(response.status, data);
                    this.setState({
                        error: response.status,
                        errorEmailObject: data
                    })
                })
            }
        });
    };

    handleCheckboxChange(event, checked) {
        console.log("handleCheckboxChange");
        this.setState({
            twoFAEnabled: checked
        });
    }

    handleTgIdChange(event) {
        console.log("handleTgIdChange, " + event.target.value);
        this.setState({
            tgId: event.target.value
        })
    }
}

Settings.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withSnackbar(withStyles(signInStyle)(Settings));