import React from "react";
import Typography from "@material-ui/core/Typography";


class Hello extends React.Component {

    style = {
        marginTop: '3rem',
        textAlign: 'center'
    };

    render() {
        const user = this.props.loggedInUser;
        return (
            user ? (
                <div>
                    <Typography variant="h3" style={this.style}>Hello {user.firstName} {user.lastName}!</Typography>
                    <Typography variant="h3" style={this.style}>You can change your authentication mode at Settings
                        section</Typography>
                </div>
            ) : (
                <div>
                    <Typography variant="h3" style={this.style}>Hello stranger!</Typography>
                    <Typography variant="h3" style={this.style}>Please Sing Up or Login with existing
                        account</Typography>
                </div>
            )
        );
    }
}

export default Hello;