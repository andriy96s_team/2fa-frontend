import React from 'react';
import PropTypes from "prop-types";
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
import {Button, FormControl, FormHelperText, Grid, Input, InputLabel, Paper} from "@material-ui/core";
import signInStyle from "../style/signInStyle"
import {getBackendUrl} from "../util/Helper";
import {withSnackbar} from "notistack";


class TgCode extends React.Component {

    state = {
        error: 0,
        errorLoginObject: {}
    };


    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };


    handleSubmitAction = event => {
        const {code} = this.state;
        event.preventDefault();

        // const body = {
        //     "username": login,
        //     "password": password
        // };

        // const currLocation = window.location;
        fetch(`${getBackendUrl()}/api/user/verifyCode/${this.props.username}/${code}`, {
            headers: {
                "Content-Type": "application/json;charset=UTF-8"
            },
            method: "GET",
            // body: JSON.stringify(body)
        }).then(response => {

            if (response.status === 200) {
                response.json().then(data => {
                    console.log("success", data);
                    localStorage.setItem("auth", JSON.stringify(data));
                    console.log(data);
                    this.props.history.push('/');
                    this.props.setValue('is2FA', false);
                    this.props.setLoggedInUser(data);
                })
            } else if (response.status === 400) {
                this.setState({
                    error: response.status,
                    errorEmailObject: {
                        reason: "Invalid code"
                    }
                })
            } else {
                response.json().then(data => {
                    console.log(response.status, data)
                    this.setState({
                        error: response.status,
                        errorEmailObject: data
                    })
                })
            }
        });
    };

    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <Grid container className={classes.gridContainer}>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <div className={classes.card}>
                                <div color="info" className={classes.cardHeader}>
                                    <div>Login</div>
                                </div>
                                <div>
                                    <form className={classes.form} onSubmit={this.handleSubmitAction}>
                                        <FormControl margin="normal" required fullWidth>
                                            <InputLabel htmlFor="code">Tg code</InputLabel>
                                            <Input
                                                id="code"
                                                name="code"
                                                onChange={this.handleChange("code")}
                                                autoFocus
                                            />
                                            {this.state.error ? (
                                                <FormHelperText
                                                    className={this.state.error === 0 ? classes.formErrorSucc : classes.formErrorFail}>
                                                    {this.state.errorEmailObject.reason}
                                                    {this.state.errorEmailObject.messages ? this.state.errorEmailObject.messages.map(item => {
                                                        return <div>{item}</div>;
                                                    }) : null}
                                                </FormHelperText>
                                            ) : (
                                                ""
                                            )}
                                        </FormControl>
                                        <div className={classes.divider}>
                                            <Button
                                                type="submit"
                                                variant="contained"
                                                className={classes.submit}
                                            >
                                                Submit
                                            </Button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

TgCode.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withSnackbar(withStyles(signInStyle)(TgCode));
