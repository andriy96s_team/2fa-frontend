import React from 'react';
import withStyles from "@material-ui/core/styles/withStyles";
import headerStyle from "../style/headerStyle"
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {Button} from "@material-ui/core";
import {LogoutCurrentUserIfExists} from "../util/Helper";

class ButtonAppBar extends React.Component {

    handleLoginClick = () => {
        this.props.history.push('/login')
    };
    handleSignupClick = () => {
        this.props.history.push('/signup')
    };
    handleLogoClick = () => {
        this.props.history.push('/')
    };
    handleLogoutClick = () => {
        LogoutCurrentUserIfExists();
        this.props.setLoggedInUser(false);
        this.props.history.push('/');
    };
    handleSettingsClick = () => {
        this.props.history.push('/settings');
    };


    render() {
        const classes = this.props;

        return (
            <div className={classes.root}>
                <AppBar position="static">
                    <Toolbar>
                        <Typography variant="h6" className={classes.title} onClick={this.handleLogoClick}>
                            2FA Test
                        </Typography>
                        {this.props.loggedInUser ? (
                                <div>
                                    <Button color="inherit" onClick={this.handleSettingsClick}>Settings</Button>
                                    <Button color="inherit" onClick={this.handleLogoutClick}>Log out</Button>
                                </div>
                            )
                            : (
                                <div>
                                    <Button color="inherit" onClick={this.handleLoginClick}>Login</Button>
                                    <Button color="inherit" onClick={this.handleSignupClick}>Sign Up</Button>
                                </div>

                            )
                        }

                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}

export default withStyles(headerStyle)(ButtonAppBar);