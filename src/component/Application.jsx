import React from "react";
import ButtonAppBar from "./ButtonAppBar";

class Application extends React.Component {

    render() {
        return (
            <div>
                <ButtonAppBar history={this.props.history} {...this.props}/>
                {this.props.children}
            </div>
        );
    }
}

export default Application;
